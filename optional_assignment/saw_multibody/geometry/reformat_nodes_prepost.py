#/usr/bin/env python

################################################################################
# Description of the code:
# 
# Reformat LS-PREPOST .g files
# 
# Author     : Ellankavi
# Date       : 28 May 2014
# Change log : 
# 
################################################################################

'''
	Reformat an LS-PREPOST generated .g file into a format usable in .k file

	The purpose of a .g is to save the nodes selected in the pre-post window. 
	The nodes are stored in rows with 10 nodes in each row.

	The .k file expects 8 nodes in a (formatted) row. 

	The purpose of this script is to do reformat the nodes from 10 in each row
	to 8 in a (formatted) row.

	SYNTAX: 
'''

import sys

def syntax_error():
	print '\tSYNTAX:'
	sys.stdout.write ('\t\t%s %s\n'%(sys.argv[0],'<.g file>'))
	sys.exit(2)

if __name__ == '__main__':

	if len(sys.argv)-1 != 1:
		syntax_error()

	inFile=sys.argv[1]
	fr=open(inFile,'r')
	nrNodes=0
	nodeList=[]

	for line in fr:
		line=line.rstrip('\r\n')
		if '*' in line:
			continue
		if 'node' in line:
			nrNodes=int(line.split(',')[1])
			continue
		else:
			nodeList.extend(map(int,line.split()))

	fr.close()

	nodeCounter=0
	newLineFlag=False
	fo=open('formatted_nodeset.k','w')	
	#fo.write('*SET_NODE_LIST\n')
	#fo.write('$#     SID       DA1       DA2       DA3       DA4    SOLVER\n\n')
	#fo.write('$#    NID1      NID2      NID3      NID4      NID5      NID6      NID7      NID8\n')
	for node in nodeList:
		fo.write('%10d'%node)
		nodeCounter+=1
		if nodeCounter%8 == 0:
			fo.write('\n')
			newLineFlag=True
		else:
			newLineFlag=False
	#if newLineFlag:
	#	fo.write('*END')
	#else:
	#	fo.write('\n*END')
	fo.close()
